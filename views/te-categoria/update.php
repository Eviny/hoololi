<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TeCategoria */

$this->title = 'Update Te Categoria: ' . $model->id_categoria;
$this->params['breadcrumbs'][] = ['label' => 'Te Categorias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_categoria, 'url' => ['view', 'id' => $model->id_categoria]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="te-categoria-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
