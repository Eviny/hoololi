<?php

/* @var $this yii\web\View */

$this->title = 'HOOLOLI';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><strong>HOOLOLI!</strong></h1>

        <p class="lead">Sua agenda de reposição mais acessível para o seu dia a dia</p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Horários</h2>

                <p>Agende os seus horários.</p>

                <p><a class="btn btn-default" href="http://localhost/hoololi/web/index.php?r=te-horarios%2Findex">Confira! &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Evento</h2>

                <p>Agende os seus eventos.</p>

                <p><a class="btn btn-default" href="http://localhost/hoololi/web/index.php?r=te-evento%2Findex">Confira! &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Categoria</h2>

                <p>Agende as sua categorias.</p>

                <p><a class="btn btn-default" href="http://localhost/hoololi/web/index.php?r=te-categoria%2Findex">Confira! &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
