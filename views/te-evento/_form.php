<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TeEvento */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="te-evento-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dia_do_evento')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dia_da_semana')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descricao')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'situacao')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'te-horarios_id')->textInput() ?>

    <?= $form->field($model, 'cod_horario')->textInput() ?>

    <?= $form->field($model, 'cod_categoria')->textInput() ?>

    

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
