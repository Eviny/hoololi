<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TeEvento */

$this->title = $model->id_evento;
$this->params['breadcrumbs'][] = ['label' => 'Te Eventos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="te-evento-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Atualizar', ['update', 'id' => $model->id_evento], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Apagar', ['delete', 'id' => $model->id_evento], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Você deseja apagar esse item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_evento',
            'descricao',
            'situacao',
            'cod_horario',
            'cod_categoria',
            [
                'attribute'=>'dia_do_evento',
                'format' => 'date',
            ],
            [
                'attribute'=>'dia_da_semana',
                'format' => 'date',
            ],
        ],
    ]) ?>

</div>
