<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TeEventoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Te Eventos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="te-evento-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Te Evento', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_evento',
            'descricao',
            'situacao',
            
            [
                'attribute'=>'dia_do_evento',
                'format' => 'date',
            ],
            [
                'attribute'=>'dia_da_semana',
                'format' => 'date',
            ],
            
            //'cod_horario',
            //'cod_categoria',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
