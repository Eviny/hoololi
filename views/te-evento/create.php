<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TeEvento */

$this->title = 'Create Te Evento';
$this->params['breadcrumbs'][] = ['label' => 'Te Eventos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="te-evento-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
