<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TeEventoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="te-evento-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_evento') ?>

    <?= $form->field($model, 'dia_do_evento') ?>

    <?= $form->field($model, 'dia_da_semana') ?>

    <?= $form->field($model, 'descricao') ?>

    <?= $form->field($model, 'situacao') ?>

    <?php // echo $form->field($model, 'cod_horario') ?>

    <?php // echo $form->field($model, 'cod_categoria') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
