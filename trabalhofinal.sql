drop database trabalhofinall;

create database trabalhofinall;

use trabalhofinall;

create table te_horarios(
id int auto_increment,
primary key (id) ,
inicio varchar(04)not null,
fim varchar(04)not null
);

create table te_categoria(
id_categoria int auto_increment,
primary key(id_categoria),
nome varchar (200) not null
);

create table te_evento(
id_evento int auto_increment,
primary key(id_evento),
dia_do_evento varchar(10)not null,
dia_da_semana varchar(20)not null,
descricao varchar(1000)not null,
situacao varchar(1000) not null,
cod_horario int,
cod_categoria int,
foreign key(cod_horario) references te_horarios(id),
foreign key(cod_categoria) references te_categoria(id_categoria)
);
