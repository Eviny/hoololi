<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "te_horarios".
 *
 * @property int $id
 * @property string $inicio
 * @property string $fim
 *
 * @property TeEvento[] $teEventos
 */
class TeHorarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'te_horarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['inicio', 'fim'], 'required'],
            [['inicio', 'fim'], 'string', 'max' => 4],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'inicio' => 'Inicio',
            'fim' => 'Fim',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeEventos()
    {
        return $this->hasMany(TeEvento::className(), ['cod_horario' => 'id']);
    }
}
