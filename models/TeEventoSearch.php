<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TeEvento;

/**
 * TeEventoSearch represents the model behind the search form of `app\models\TeEvento`.
 */
class TeEventoSearch extends TeEvento
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_evento', 'cod_horario', 'cod_categoria'], 'integer'],
            [['dia_do_evento', 'dia_da_semana', 'descricao', 'situacao'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TeEvento::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_evento' => $this->id_evento,
            'cod_horario' => $this->cod_horario,
            'cod_categoria' => $this->cod_categoria,
        ]);

        $query->andFilterWhere(['like', 'dia_do_evento', $this->dia_do_evento])
            ->andFilterWhere(['like', 'dia_da_semana', $this->dia_da_semana])
            ->andFilterWhere(['like', 'descricao', $this->descricao])
            ->andFilterWhere(['like', 'situacao', $this->situacao]);

        return $dataProvider;
    }
}
