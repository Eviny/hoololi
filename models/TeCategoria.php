<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "te_categoria".
 *
 * @property int $id_categoria
 * @property string $nome
 *
 * @property TeEvento[] $teEventos
 */
class TeCategoria extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'te_categoria';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'required'],
            [['nome'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_categoria' => 'ID',
            'nome' => 'Nome',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeEventos()
    {
        return $this->hasMany(TeEvento::className(), ['cod_categoria' => 'id_categoria']);
    }
}
