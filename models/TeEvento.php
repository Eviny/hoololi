<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "te_evento".
 *
 * @property int $id_evento
 * @property string $dia_do_evento
 * @property string $dia_da_semana
 * @property string $descricao
 * @property string $situacao
 * @property int $cod_horario
 * @property int $cod_categoria
 *
 * @property TeHorarios $codHorario
 * @property TeCategoria $codCategoria
 */
class TeEvento extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'te_evento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dia_do_evento', 'dia_da_semana', 'descricao', 'situacao'], 'required'],
            [['cod_horario', 'cod_categoria'], 'integer'],
            [['dia_do_evento'], 'string', 'max' => 10],
            [['dia_da_semana'], 'string', 'max' => 20],
            [['descricao', 'situacao'], 'string', 'max' => 1000],
            [['cod_horario'], 'exist', 'skipOnError' => true, 'targetClass' => TeHorarios::className(), 'targetAttribute' => ['cod_horario' => 'id']],
            [['cod_categoria'], 'exist', 'skipOnError' => true, 'targetClass' => TeCategoria::className(), 'targetAttribute' => ['cod_categoria' => 'id_categoria']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_evento' => 'ID',
            'dia_do_evento' => 'Dia do Evento',
            'dia_da_semana' => 'Dia da Semana',
            'descricao' => 'Descrição',
            'situacao' => 'Situação',
            'cod_horario' => 'Horário',
            'cod_categoria' => 'Categoria',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodHorario()
    {
        return $this->hasOne(TeHorarios::className(), ['id' => 'cod_horario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodCategoria()
    {
        return $this->hasOne(TeCategoria::className(), ['id_categoria' => 'cod_categoria']);
    }
}
